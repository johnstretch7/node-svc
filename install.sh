#!/bin/bash
set -e  # exit immediately if anything returns non-zero. See https://www.javatpoint.com/linux-set-command

echo "  ----- download and initialize app -----  "

sudo apt-get install nano
sudo apt-get install -y git
git clone https://gitlab.com/CharlesTBetz/node-svc.git
cd node-svc
git checkout 02

npm install express --save
npm install node-fetch --save
npm install
